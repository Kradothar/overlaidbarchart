/*globals define*/
define( ["qlik", "jquery", "d3", "text!./style.css", "./properties"], function ( qlik, $, d3, cssContent, props ) {
	'use strict';
	$( "<style>" ).html( cssContent ).appendTo( "head" );
	  var determineMinMax = function (data, qMeasureInfo){
		  var minMax = {min: null, max: null}; 
		  data.forEach(function (d){
			  var summedValue = 0;
			  qMeasureInfo.forEach( function (measure, idx){
				  if ( d[measure.qFallbackTitle] &&  d[measure.qFallbackTitle].val){
				  	if(minMax.max===null ||  d[measure.qFallbackTitle].val > minMax.max){
						minMax.max =  d[measure.qFallbackTitle].val;
				  	}
				  	if(minMax.min===null ||  d[measure.qFallbackTitle].val < minMax.min){
					  	minMax.min =  d[measure.qFallbackTitle].val;
				  	}
				   }
				   if ( d[measure.qFallbackTitle] &&  d[measure.qFallbackTitle].nVal){
				  	if(minMax.min===null || d[measure.qFallbackTitle].nVal || d[measure.qFallbackTitle].nVal< minMax.min){
					  	minMax.min =  d[measure.qFallbackTitle].nVal;
				  	}
				}
			  } )
		  });
		  return minMax;
	  }
	  
	var lineFunction = d3.svg.line()
		.x(function(d) { return d.x; })
		.y(function(d) { return d.y; })
		.interpolate("linear");

	return {
		initialProperties: {
			qHyperCubeDef: {
				qDimensions: [],
				qMeasures: [],
				qInitialDataFetch: [{
					qWidth: 10,
					qHeight: 1000
				}]
			}
		},
		definition: props,
		snapshot: {
			canTakeSnapshot: true
		},
		paint: function ( $element, layout ) {
			var _this = this;
			 
            var id = "container_" + layout.qInfo.qId;
			// Define Margins
			var margin = {top: 80, bottom: 80, left: 60, right: 10};
			
			var width = $element.width() - margin.left - margin.right;  
            var height = $element.height() - margin.top - margin.bottom; 
			
			// Use Lediant's corporate palette
			var colours = [
				{R: 0, B: 75, G: 135},
				{R: 255, B: 158, G: 27},
				{R: 0, B: 181, G: 226},
				{R: 254, B: 80, G: 0},
			]
			
			var bar = {width: layout.props.barWidth, pad:2};			
            if (document.getElementById(id)) {  
                $("#" + id).empty();  
            }  
            else {  
                $element.append($('<div />;').attr("id", id).width(width).height(height));  
            } 
			var qMatrix = layout.qHyperCube.qDataPages[0].qMatrix;  
			var qMeasureInfo = layout.qHyperCube.qMeasureInfo;
			
			var cutIdx = -1;
			
			var data = qMatrix.map(function(d, count) {  
				var dObj = {};
				
				d.forEach(function(element, idx){
					if(idx == 0){
						dObj["Dim"] = element.qText;
						if(!dObj["Dim"] || dObj["Dim"] == '-' ){
							cutIdx = count;
						}
					} else {
						var measure = qMeasureInfo[idx-1].qFallbackTitle;
						// Exclude if value is less than zero
						if (element.qNum>=0){
							dObj[measure] = {val: element.qNum};
						} else {
							dObj[measure] = {nVal: element.qNum};
						}
						
					}
				})
				return dObj;
			});
			
			if (cutIdx != -1){
				data.splice(cutIdx, 1);
			}
			var minMax = determineMinMax(data, qMeasureInfo);
			
			var doVisualisation = function(id, width, height, data, minMax, bar, qMeasureInfo) {  
			
			d3.select("body").append("svg");
				var svg = d3.select("#"+id)  
					.append("svg")  
					.attr("width", width + margin.left + margin.right)
					.attr("height", height + margin.top + margin.bottom)
 					.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")");  
				
				
				var noPts = data.length;
				
				var yScale = d3.scale.linear()
                    .domain([minMax.min, minMax.max])
                    .range([0 , height]);
				
				var xScale = d3.scale.ordinal()
                    .domain([0, noPts])
                    .range([0 , width]);
				
				
				
				///////////////////////////////////////////////////
				// X Axis labels
				 var txt = svg.selectAll("text")
					.data(data)
					.enter();
					
				 txt.append("text")
				   .attr("y", function (d){					
				   return 0; //height - margin.bottom;
				 })
				   .attr("x", function(d, i){
				   return 0; 
				 })
				 //.attr("dy", ".35em")
				   .attr("transform", function (d, i){
				   var x = ((i*(width/noPts))) + 13;
				   var y =  height + 20;
				   return "translate(" + x + "," + y + ") rotate(-45)";
				 })
				   .style("text-anchor", "end")
				   .text(function(d) {
				   return d.Dim;
				 });

				var xAxis = d3.svg.axis()
                  .orient("bottom")
                  .scale(xScale);
				  
				  var zeroPos = yScale (0);
				  
				  svg.append("g")
    				.attr("class", "axis")  //Assign "axis" class
    				.attr("transform", "translate(0," + (height - zeroPos) + ")")
    				.call(xAxis);
				
				///////////////////////////////////////////////
				// Legend
				if (layout.props.showLabels){
					
					var legend = svg.append("g");
					legend.selectAll("circle")
						.data(qMeasureInfo)
						.enter()
						.append("circle")
						.attr("cx", function (d,i){
							return width - 200;
						})
						.attr("cy", function (d,i){
							return (i * 20) + 10;
						})
						.attr("r", 10)
						.attr("fill", function(d, i) {
							return "rgb("+ colours[i].R  + ", " + colours[i].B + ", " + colours[i].G  + ")";
						});
					legend.selectAll("text")
						.data(qMeasureInfo)
						.enter()
						.append("text")
						.attr("x", function (d,i){
							return width - 180;
						})
						.attr("y", function (d,i){
							return (i * 20) + 14;
						})
						.text(function (d,i){
							return d.qFallbackTitle;
						})
						.attr("fill", function(d, i) {
							return "rgb("+ colours[i].R  + ", " + colours[i].B + ", " + colours[i].G  + ")";
						});	
				}
				
				// Functions for determining bar size and position
				var barX = function (i){
					return i*(width/noPts);
				};
				
				var barYPos = function (val){
					return height - yScale(val);
				};
				var xIntercept = function (){
					return height - yScale(0);
				};
				
				var barHeight = function(val){
					return Math.abs(yScale(val) - yScale(0)) ;
				};
				
				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				// States loops through positive values, switches the arrays around, then loops through negative values
				var states = ["val", "nVal"];
				
				states.forEach( function(state){
				///////////////////////////////////////////////////
				// Loop through each measure adding bars for each
				qMeasureInfo.forEach(function(measure, idx){
				
					// Main rectangles
					var g = svg.append('g')
						g.selectAll("rect")
						.data(data)
					   .enter()
					   .append("rect")
					   .attr("x", function(d, i){
							return barX(i);
						})
					   .attr("y", function(d, i){
					   		if ( d[measure.qFallbackTitle] &&  d[measure.qFallbackTitle][state]){
								var val = d[measure.qFallbackTitle][state];
								
								if (val>0){
									return barYPos(val) ;
								} else {
									return xIntercept();
								}
							} else {
								return 0;
							}
							
						} )
					   .attr("width", bar.width)
					   .attr("height", function(d){
					   		if ( d[measure.qFallbackTitle] &&  d[measure.qFallbackTitle][state]){
					   			var val = d[measure.qFallbackTitle][state];
								return barHeight(val);
							} else {
								return 0;
							}
					   })
					   .attr("fill", function(d, i) {
							return "rgb("+ colours[idx].R  + ", " + colours[idx].B + ", " + colours[idx].G  + ")";
						});
				});
				
					
				///////////////////////////////////////////////
				// Numeric Labels
				if (layout.props.showLabels){
					//Numeric labels on chart need to be appropriately spaced
					var t = qMeasureInfo.length;
					var spread = layout.props.labelSpread;
				
					
					qMeasureInfo.forEach(function(measure, idx){
						var ySpread = (idx-2) * spread/t;
				  		
						txt.append("text")
						.attr("y", function (d){					
							return 0; //height - margin.bottom;
				  		})
						.attr("x", function(d, i){
							return 0;
				 		})
						.attr("fill", "Gray")
				 		.attr("transform",function (d, i){
							if(d[qMeasureInfo[idx].qFallbackTitle] && d[qMeasureInfo[idx].qFallbackTitle][state]) {
								var val = d[measure.qFallbackTitle][state];
								var xInt = xIntercept();
								var x =  barX(i) + layout.props.labelX;
								var y =  val>0?barYPos(val) + ySpread - 4:xInt + barHeight(val) - ySpread  ;
								
								// Look for y in a certain range to look for x axis intercept
								if(y > (xInt - 5) && y < (xInt + 15)){
									// If x axis is hit, move the label, either up or down
									if(y > (xInt + 4)){
										y = y + 7
									} else {
										y = y - 5;
									}
								}
								
								return "translate(" + x + "," + y + ")";
							} else {
								return "";
							}
						})
					 	.style("text-anchor", "start")
					  	.text(function(d) {
							if(d[qMeasureInfo[idx].qFallbackTitle] && d[qMeasureInfo[idx].qFallbackTitle][state]) {
								var val = (Math.round(d[qMeasureInfo[idx].qFallbackTitle][state] /10000) /100) + "M";
								return val;
							} else {
								return "";
							}
				  		});
						
						/////////////////////////////////////////////////////////////
						// Make a collection of indices for the label lines
						var labLines = data.map (function(d, i){
							if(d[qMeasureInfo[idx].qFallbackTitle] && d[qMeasureInfo[idx].qFallbackTitle][state]) {
								var val = d[measure.qFallbackTitle][state];
								var xInt = xIntercept();
								var x1 =  barX(i) + layout.props.labelX - 3;
								var y1 =  val>0?barYPos(val) + ySpread - 4:xInt + barHeight(val) - ySpread ;
								
								// Look for y in a certain range to look for x axis intercept
								if(y1 > (xInt - 5) && y1 < (xInt + 15)){
									// If we find an intercept, move the label out of the way
									if(y1 > (xInt + 4)){
										y1 = y1 + 7
									} else {
										y1 = y1 - 5;
									}
								}
								
								if(val<=0){y1 = y1 - 4};
								y1 = y1 - 4
								
								var x2 = (barX(i) + bar.width);
								var y2 =  val>0?barYPos(val):xInt + barHeight(val);

								return [{x: x1, y: y1}, {x: x2, y: y2}];
							} else {
								return [{x: 0, y: 0}, {x: 0, y: 0}];
							}
						});

						var linesContainer = svg.append("g");
						
						labLines.forEach(function (singleLine, i){
							var lineFunction = d3.svg.line()
                         		.x(function(d) { return d.x; })
                         		.y(function(d) { return d.y; })
                         		.interpolate("linear");
							
							linesContainer.append("path")
                            	.attr("d", lineFunction(singleLine))
                            	.attr("stroke", "Gray")
                            	.attr("stroke-width", 1)
                            	.attr("fill", "none")
								.style("stroke-dasharray", "5 5");

						});
						
					});
					
						
					}
					
					// Do the same in reverse
					qMeasureInfo.reverse();
					colours.reverse();
					
				})
				
				
				
				
			}
				
			doVisualisation(id, width, height, data, minMax, bar, qMeasureInfo);
			
			return qlik.Promise.resolve();
		}
		
		
	};
} );

// JavaScript
define( [], function () {
	'use strict';
	// *****************************************************************************
	// Dimensions & Measures
	// *****************************************************************************
	var dimensions = {
		uses: "dimensions",
		min: 1,
		max: 1
	};
	var measures = {
		uses: "measures",
		min: 1,
		max: 4
	};
		// Bar width definition
	var barWidth = {
		type: "number",
		component: "slider",
		min: 2,
		max: 100,
		step: 2,
		defaultValue: 50,
		ref: "props.barWidth",
		label: "Bar Width"
	};
	var showLabels = {
	  type: "boolean",
	  label: "Show labels",
	  ref: "props.showLabels",
	  defaultValue: true
	};
	
	var labelSpread = {
		type: "number",
		component: "slider",
		min: 2,
		max: 100,
		step: 2,
		defaultValue: 50,
		ref: "props.labelSpread",
		label: "Label Spread"
	};
	
	var labelX = {
		type: "number",
		component: "slider",
		min: 0,
		max: 100,
		step: 2,
		defaultValue: 50,
		ref: "props.labelX",
		label: "Label X"
	};
	
	var showKey = {
	  type: "boolean",
	  label: "Show Key",
	  ref: "props.showKey",
	  defaultValue: true
	};
	// *****************************************************************************
	// Appearance section
	// *****************************************************************************
	var appearanceSection = {
		uses: "settings",
		items: {
			barWidth: barWidth,
			labelSpread: labelSpread,
			labelX: labelX,
			showLabels: showLabels,
			showKey: showKey
		}
	};
	
	// *****************************************************************************
	// Main properties panel definition
	// Only what is defined here is returned from properties.js
	// *****************************************************************************
	return {
		type: "items",
		component: "accordion",
		items: {
			dimensions: dimensions,
			measures: measures,
			appearance: appearanceSection
		}
	};
});